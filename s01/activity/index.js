class Contractor {
	constructor(name,email,contactNo){
		this.name = name;
		this.email = email;
		this.contactNo = contactNo;
	}

	getContractorDetails(){
		console.log(`Name: ${this.name} \nEmail: ${this.email} \nContact No: ${this.contactNo} `);
		//return this;
	}
}

let contractor1 = new Contractor('Utra Manpower Services','ultra@manpower.com','09167890123');
contractor1.getContractorDetails();

class Subcontractor extends Contractor{
	constructor(name,email,contactNo,specializations){
		super(name,email,contactNo);
		this.specializations = specializations;
	}

	getSubConDetails(){
		//let spec = JSON.stringify(this.specializations);
		//let specString = ''
		//this.specializations.forEach(el=> specString += el)
		this.getContractorDetails();
		console.log(`${this.specializations.join(',')}`);
//		return this;
	}
		
}	

let subCont1 = new Subcontractor('Ace Bros','acebros@mail.com','09151234567',['gardens','industrial']);
let subCont2 = new Subcontractor('LitC corp','litc@mail.com','091512456789',["schools","hospitals","bakeries","libraries"]);

subCont1.getSubConDetails();
subCont2.getSubConDetails();