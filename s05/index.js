class Request{
	constructor(requesterEmail,content,dateRequested){
		this.requesterEmail = requesterEmail;
		this.content = content;
		this.dateRequested = dateRequested;
	}
}

class Person{
	constructor(){
		if(this.constructor === Person){
			throw new Error(
				"Object cannot be created from an abstraction class Person"
			);
		}

		if(this.getFullName === undefined){
			throw new Error(
				"Class must implement getFullName method"
			);
		}
		if(this.login === undefined){
			throw new Error(
				"Class must implement login method"
			);
		}
		if(this.logout === undefined){
			throw new Error(
				"Class must implement logout method"
			);
		}

		if(this.constructor === Employee){
			if(this.addRequest === undefined){
				throw new Error(
					"class must implement addRequest method"
				)
			}
		}

		if(this.constructor === TeamLead){
			if(this.addMember === undefined){
				throw new Error(
					"class must implement addMember method"
				)
			}
			if(this.checkRequests === undefined){
				throw new Error(
					"class must implement checkRequests method"
				)
			}
		}

		if(this.constructor === Admin){
			if(this.addTeamLead === undefined){
				throw new Error(
					"class must implement addTeamLead method"
				)
			}
			if(this.deactivateTeam === undefined){
				throw new Error(
					"class must implement deactivateTeam method"
				)
			}
		}







	}
}

class Employee extends Person{
	#firstName;
	#lastName;
	#email;
	#department;
	#isActive;
	#requests
	constructor(firstName,lastName,email,department,isActive){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = isActive;
		this.#requests = [];
	}
	getFullName(){
		return `${this.#firstName} ${this.#lastName}`
	}
	login(){
		return `${this.#firstName} ${this.#lastName} has logged in`
	}
	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out`
	}
	addRequest(){
		let request = new Request(this.#email,'Myrequest', new Date);
		this.#requests.push(request);
	}

	//setter
	setFirstName(firstName){
		this.#firstName = firstName;
	}
	setLastName(lastName){
		this.#lastName = lastName;
	}
	setEmail(email){
		this.#email = email;
	}
	setDepartment(department){
		this.#department = department;
	}
	setIsActive(isActive){
		this.#isActive = isActive;
	}
	setRequests(requests){
		this.#requests = requests
	}

	//getters
	getFirstName(){
		return this.#firstName
	}
	getLastName(){
		return this.#lastName
	}
	getEmail(){
		return this.#email
	}
	getDepartment(){
		return this.#department
	}
	getIsActive(){
		return this.#isActive
	}
	getRequests(){
		return `${JSON.stringify(this.#requests)}`
	}
}

class TeamLead extends Person{
	#firstName;
	#lastName;
	#email;
	#department;
	#isActive;
	#members
	constructor(firstName,lastName,email,department,isActive){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = isActive;
		this.#members = [];
	}
	getFullName(){
		return `${this.#firstName} ${this.#lastName}`
	}
	login(){
		return `${this.#firstName} ${this.#lastName} has logged in`
	}
	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out`
	}

	addMember(employee){
		this.#members.push(employee);
	}
	checkRequests(email){
		let foundMember = this.#members.find(member => member.getEmail() == email)
		if(foundMember == undefined){
			return 'no matches found please provide another email'
		} else{
			return `${foundMember.getRequests()}`
		}
	}

	//setter
	setFirstName(firstName){
		this.#firstName = firstName;
	}
	setLastName(lastName){
		this.#lastName = lastName;
	}
	setEmail(email){
		this.#email = email;
	}
	setDepartment(department){
		this.#department = department;
	}
	setIsActive(isActive){
		this.#isActive = isActive;
	}
	setMembers(members){
		this.#members = members
	}

	//getters
	getFirstName(){
		return this.#firstName
	}
	getLastName(){
		return this.#lastName
	}
	getEmail(){
		return this.#email
	}
	getDepartment(){
		return this.#department
	}
	getIsActive(){
		return this.#isActive
	}
	getMembers(){
		return this.#members
	}
}

class Admin extends Person{
	#firstName;
	#lastName;
	#email;
	#department;
	#isActive;
	#teamLeads
	constructor(firstName,lastName,email,department,isActive){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = isActive;
		this.#teamLeads = [];
	}
	getFullName(){
		return `${this.#firstName} ${this.#lastName}`
	}
	login(){
		return `${this.#firstName} ${this.#lastName} has logged in`
	}
	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out`
	}

	addTeamLead(teamLead){
		this.#teamLeads.push(teamLead);
	}
	/*checkRequests(email){
		foundMember = this.#members.find(member => member.getEmail() == email)
		if(foundMember == undefined){
			return 'no matches found please provide another email'
		} else{
			return `${foundMember.getRequests()}`
		}
	}*/

	deactivateTeam(email){
		let foundTeamLead = this.#teamLeads.find(member => member.getEmail() == email)
		if(foundTeamLead == undefined){
			return 'no matches found please provide another email'
		} else{
			// return `${foundTeamLead.getRequests()}`
			foundTeamLead.setIsActive(false);
			let members = foundTeamLead.getMembers();
			members.forEach(member=> member.setIsActive(false));
		}
	}

	//setter
	setFirstName(firstName){
		this.#firstName = firstName;
	}
	setLastName(lastName){
		this.#lastName = lastName;
	}
	setEmail(email){
		this.#email = email;
	}
	setDepartment(department){
		this.#department = department;
	}
	setIsActive(isActive){
		this.#isActive = isActive;
	}
	setTeamLeads(teamLeads){
		this.#teamLeads = teamLeads
	}

	//getters
	getFirstName(){
		return this.#firstName
	}
	getLastName(){
		return this.#lastName
	}
	getEmail(){
		return this.#email
	}
	getDepartment(){
		return this.#department
	}
	getIsActive(){
		return this.#isActive
	}
	getTeamLeads(){
		return this.#teamLeads
	}
}




///tests

let employee1 = new Employee();
employee1.setFirstName('John');
employee1.setLastName('sSmith');
employee1.setEmail('john@mail.com');
employee1.setDepartment('Accounting');
employee1.setIsActive(true);

let teamLead1 = new TeamLead();
teamLead1.setFirstName('Jane');
teamLead1.setLastName('sSmith');
teamLead1.setEmail('jane@mail.com');
teamLead1.setDepartment('Accounting');
teamLead1.setIsActive(true);

let admin1 = new Admin();
admin1.setFirstName('Joe');
admin1.setLastName('sSmith');
admin1.setEmail('joe@mail.com');
admin1.setDepartment('Accounting');
admin1.setIsActive(true);

console.log(employee1.getFullName())
console.log(employee1)
console.log(employee1.getIsActive())

console.log(teamLead1.getFullName())
console.log(teamLead1)
console.log(teamLead1.getIsActive())

console.log(admin1.getFullName())
console.log(admin1)
console.log(admin1.getTeamLeads())


employee1.addRequest();
teamLead1.addMember(employee1);

console.log(teamLead1.checkRequests('john@mail.com'))
admin1.addTeamLead(teamLead1);
console.log('usinggg deactivateTeam')
admin1.deactivateTeam('jane@mail.com');
console.log(teamLead1.getFullName())
console.log(teamLead1.getIsActive())
console.log(employee1.getFullName())
console.log(employee1.getIsActive())


/*

jsdfnvsfj



*/