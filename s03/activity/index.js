class RegularShape{
	constructor(){
		
		if(this.constructor === RegularShape){
			throw new Error(
				"Object cannot be created from an abstraction class RegularShape"
			);
		}

		if(this.getPerimeter === undefined){
			throw new Error(
				"Class must implement getPerimeter method"
			);
		}
		if(this.getArea === undefined){
			throw new Error(
				"Class must implement getArea method"
			);
		}
	}

	
}

class Square extends RegularShape{

	constructor(noSides,length){
		super();
		this.noSides = noSides;
		this.length = length;
	}

	getPerimeter(){
		return `The perimeter of the square is ${this.length * 4}`
	}
	getArea(){
		return `The area of square is ${this.length ** 2}`
	}
}

const shape1 = new Square(4,16);
console.log(shape1.getPerimeter());
console.log(shape1.getArea());


//=================================================================



class Food{
	constructor(name,price){
		this.name = name;
		this.price = price;
		if(this.constructor === Food){
			throw new Error(
				"Object cannot be created from an abstraction class Food"
			);
		}

		if(this.getName === undefined){
			throw new Error(
				"Class must implement getName method"
			);
		}
	}
}

class Vegetable extends Food{
	constructor(name,breed,price){
		super(name,price);
		this.breed = breed;
	}

	getName(){
		return `${this.name} is of ${this.breed} variety and is priced at ${this.price} pesos`
	}
}

const vegetable1 = new Vegetable('Pechay','Native',25);
console.log(vegetable1.getName());


//========================================================



class Equipment{
	constructor(equipmentType,model){
		this.equipmentType = equipmentType;
		this.model = model;
		if(this.constructor === Equipment){
			throw new Error(
				"Object cannot be created from an abstraction class Equipment"
			);
		}

		if(this.printInfo === undefined){
			throw new Error(
				"Class must implement printInfo method"
			);
		}

	}

	// printInfo(){
	// 	return `Info: ${this.equipmentType}`;
	// }
}

class Bulldozer extends Equipment{
	constructor(equipmentType,model,bladeType){
		super(equipmentType,model);
		
		this.bladeType = bladeType;

	}

	printInfo(){
		return `Info: ${this.equipmentType} \nThe bulldozer ${this.model} has a ${this.bladeType} blade `
	}
}


class TowerCrane extends Equipment{
	constructor(equipmentType,model,hookRadius,maxCapacity){
		super(equipmentType,model);
		//this.model = model;
		this.hookRadius = hookRadius;
		this.maxCapacity = maxCapacity;

	}
	printInfo(){
		return `Info: ${this.equipmentType} \nThe tower crane ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`;
	}
}


const bulldozer1 = new Bulldozer('bulldozer','Brute','Shovel');


const towercrane1 = new TowerCrane('tower crane','Pelican',100,1500);

console.log(bulldozer1.printInfo());
console.log(towercrane1.printInfo());