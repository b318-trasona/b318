class RegularShape{
	constructor(){
		
		if(this.constructor === RegularShape){
			throw new Error(
				"Object cannot be created from an abstraction class RegularShape"
			);
		}

		if(this.getPerimeter === undefined){
			throw new Error(
				"Class must implement getPerimeter method"
			);
		}
		if(this.getArea === undefined){
			throw new Error(
				"Class must implement getArea method"
			);
		}
	}

	
}

class Square extends RegularShape{
		#noSides;
		#length;

	constructor(noSides,length){
		super();
		this.#noSides = noSides;
		this.#length = length;
	}

	getPerimeter(){
		return `The perimeter of the square is ${this.#length * this.#noSides}`
	}
	getArea(){
		return `The area of square is ${this.#length ** 2}`
	}


	//setters
	setLength(length){
		this.#length = length;
	}

	setNoSides(noSides){
		this.#noSides = noSides;
	}

	//getters
	getLength(){
		return this.#length;
	}
	getNoSides(){
		return this.#noSides;
	}
}

class Triangle extends RegularShape{
	#noSides;
	#length;

	constructor(noSides,length){
		super();
		this.#noSides = noSides;
		this.#length = length;
	}

	getPerimeter(){

		return `The perimeter of the triangle is ${this.#length * this.#noSides}`
	}

	getArea(){
		 
		return `The area of the triangle is ${((this.#length ** 2) * 0.4330127018922193).toFixed(3)}`
	}

	//setters
	setLength(length){
		this.#length = length;
	}

	setNoSides(noSides){
		this.#noSides = noSides;
	}

	//getters
	getLength(){
		return this.#length;
	}
	getNoSides(){
		return this.#noSides;
	}
}

const square1 = new Square();
square1.setNoSides(4);
square1.setLength(65);
console.log(square1.getLength());
console.log(square1.getPerimeter());

const triangle1 = new Triangle();
triangle1.setNoSides(3);
triangle1.setLength(15);
console.log(triangle1.getLength());
console.log(triangle1.getArea());

//================================================

class User{
	constructor(){
		if(this.constructor === User){
			throw new Error(
				"Object cannot be created from an abstraction class User"
			);
		}

		if(this.login === undefined){
			throw new Error(
				"Class must implement login method"
			);
		}

		if(this.register === undefined){
			throw new Error(
				"Class must implement register method"
			);
		}

		if(this.logout === undefined){
			throw new Error(
				"Class must implement logout method"
			);
		}
	}
}

class RegularUser extends User{
	#name;
	#email;
	#password;
	constructor(name,email,password){
		super();
		this.#name = name;
		this.#email = email;
		this.#password = password;
	}
	login(){
		return `${this.#name} has logged in.`;
	}
	register(){
		return `${this.#name} has registered.`;
	}
	logout(){
		return `${this.#name} has logged out`;
	}


	browseJobs(){
		return `There are 10 jobs found`
	}

	//setter
	setName(name){
		this.#name = name;
	}
	setEmail(email){
		this.#email = email;
	}
	setPassword(password){
		this.#password = password;
	}

	//getter
	getName(){
		return this.#name;
	}
	getEmail(){
		return this.#email;
	}
	getPassword(){
		return this.#password;
	}

}

const regUser1 = new RegularUser();
regUser1.setName('Dan');
regUser1.setEmail('dan@mail.com');
regUser1.setPassword('Dan12345');
console.log(regUser1.register());
console.log(regUser1.login());
console.log(regUser1.browseJobs());
console.log(regUser1.logout());

class Admin extends User{
	#name;
	#email;
	#password;
	#hasAdminExpired;
	constructor(name,email,password,hasAdminExpired){
		super();
		this.#name = name;
		this.#email = email;
		this.#password = password;
		this.#hasAdminExpired = hasAdminExpired
	}
	login(){
		return `Admin ${this.#name} has logged in.`;
	}
	register(){
		return `Admin ${this.#name} has registered.`;
	}
	logout(){
		return `Admin ${this.#name} has logged out`;
	}


	postJob(){
		return `Job added to site`
	}

	//setter
	setName(name){
		this.#name = name;
	}
	setEmail(email){
		this.#email = email;
	}
	setPassword(password){
		this.#password = password;
	}
	setHasAdminExpired(hasAdminExpired){
		this.#hasAdminExpired = hasAdminExpired;
	}

	//getter
	getName(){
		return this.#name;
	}
	getEmail(){
		return this.#email;
	}
	getPassword(){
		return this.#password;
	}
	getHasAdminExpired(){
		return this.#hasAdminExpired;
	}

}

const admin = new Admin();
admin.setName('Joe');
admin.setEmail('admin_joe@mail.com');
admin.setPassword('joe12345');
admin.setHasAdminExpired(false);
console.log(admin.register());
console.log(admin.login());
console.log(admin.getHasAdminExpired());
console.log(admin.postJob());
console.log(admin.logout());