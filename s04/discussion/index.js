class Person{
	constructor(){
		if(this.constructor === Person){
			throw new Error(
				"Object cannot be created from an abstraction class Person"
			);
		}

		if(this.getFullName === undefined){
			throw new Error(
				"Class musst implement getFullName method"
			);
		}

		
	}
}

class Employee extends Person{
	//Encapsulation ,

	#firstName;
	#lastName;
	#employeeId;

	constructor(firstName,lastName,employeeId){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#employeeId = employeeId;
	}

	//getters

	getFullName(){
		return `${this.#firstName} ${this.#lastName} has employeeId of ${this.#employeeId}`
	}

	getFirstName(){
		return `First Nam: ${this.#firstName}`
	}

	getLastName(){
		return `Last Name: ${this.#lastName}`
	}

	getEmployeeId(){
		return this.#employeeId;
	}

	//setter

	setFirstName(firstName){
		this.#firstName = firstName;
	}

	setLastName(lastName){
		this.#lastName = lastName;
	}

	setEmployeeId(employeeId){
		this.#employeeId = employeeId;
	}
}

const employeeA = new Employee('John','Smith','Em-001')

console.log(employeeA.firstName);
console.log(employeeA.getFirstName());


//Direct access with field/property firstName will return undefined because the property is private

employeeA.firstName = 'David';
console.log(employeeA.getFirstName());
console.log(employeeA.setFirstName('David'));
console.log(employeeA.getFirstName());
console.log(employeeA.getFullName());


const employeeB = new Employee();
console.log(employeeB.getFullName());
employeeB.setFirstName("Jill");
employeeB.setLastName('Hill');
employeeB.setEmployeeId('em-002');
console.log(employeeB.getFullName());