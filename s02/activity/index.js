//Activity 1
class Equipment{
	constructor(equipmentType){
		this.equipmentType = equipmentType;
	}

	printInfo(){
		return `Info: ${this.equipmentType}`;
	}
}

class Bulldozer extends Equipment{
	constructor(equipmentType,model,bladeType){
		super(equipmentType);
		this.model = model;
		this.bladeType = bladeType;

	}

	printInfo(){
		return `${super.printInfo()} \nThe bulldozer ${this.model} has a ${this.bladeType} blade `
	}
}

let bulldozer1 = new Bulldozer('bulldozer','Brute','Shovel');
console.log(bulldozer1.printInfo());

class TowerCrane extends Equipment{
	constructor(equipmentType,model,hookRadius,maxCapacity){
		super(equipmentType);
		this.model = model;
		this.hookRadius = hookRadius;
		this.maxCapacity = maxCapacity;

	}
	printInfo(){
		return `${super.printInfo()} \nThe tower crane ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`;
	}
}

let towercrane1 = new TowerCrane('tower crane','Pelican',100,1500);
console.log(towercrane1.printInfo());

class BackLoader extends Equipment{
	constructor(equipmentType,model,type,tippingLoad){
		super(equipmentType);
		this.model = model;
		this.type = type;
		this.tippingLoad = tippingLoad;
	}

	printInfo(){
		return `${super.printInfo()} \nThe loader ${this.model} is a ${this.type} loader and has a tipping load of ${this.tippingLoad} lbs.`;
	}
}

let backLoader1 = new BackLoader('back loader', 'Turtle','hydraulic',1500);
console.log(backLoader1.printInfo());

// ===========================================
// ACtivity 2

class RegularShape{
	constructor(noSides,length){
		this.noSides = noSides;
		this.length = length;
	}

	getPerimeter(){
		return `the perimeter is `
	}
	getArea(){
		return `the area is `
	}
}

class Triangle extends RegularShape{
	getPerimeter(){

		return super.getPerimeter() + `${this.length * 3}`
	}

	getArea(){
		return super.getArea() + `${(this.length ** 2) * 0.4330127018922193}`
	}
}

let triangle1 = new Triangle(3,10);
console.log(triangle1.getPerimeter());
console.log(triangle1.getArea());

class Square extends RegularShape{
	getPerimeter(){
		return super.getPerimeter() + `${this.length * 4}`
	}
	getArea(){
		return super.getArea() + `${this.length ** 2}`
	}
}

let square1 = new Square(4,12);
console.log(square1.getPerimeter());
console.log(square1.getArea());