//Polymorphism
//involves overriding, where the method in subClass overrides the implementation of a method with the same name in its superclass

class Person{
	constructor(firstName,lastName){
		this.firstName = firstName;
		this.lastName = lastName;
	}

	getFullName(){
		return `The persons name is ${this.firstName} ${this.lastName}`
	}
}

class Employee extends Person{
	constructor(employeeId,firstName,lastName){
		super(firstName,lastName);
		
		this.employeeId = employeeId;
	}

	//Overriding
	getFullName(){
		return super.getFullName() + ` with employeeId ${this.employeeId}`

	}

}

const employeeA = new Employee('em-004','John','Smith');
console.log(employeeA.getFullName());

class TeamLead extends Employee{
	getFullName(){
		return super.getFullName() + ` and he/she is a Team lead`
	}
}

const teamLead = new TeamLead('tl-001','Jane','smith');
console.log(teamLead.getFullName());